#include <linux/module.h>
#include <linux/kernel.h>

// Para inicializar y registrar el driver
#include <linux/cdev.h>

// Da acceso a las operaciones de un driver
#include <linux/fs.h>

// para la función copy_to_user, copy_from user
#include <linux/uaccess.h>
// Esta función devuelve 0 si copia al búfer sin errores y si se pasa
// devuelve en nº de bytes no copiados

// Define la versión del kernel
#include <linux/version.h>
#include <linux/string.h>

#include <linux/miscdevice.h>

#define NOMB_DRIVER "CRC32Hasher"
#define DRIVER_CLASS "CRC32Class"
// Máxima longitud del bufer de la zona de usuario
#define LONG_BUF 512
#define TAM_CADENA 9

static char hexadecimal[TAM_CADENA];
static unsigned int bytesEnBufer = 0;

// Invierte los bites de un número entero sin signo
static unsigned reverse(unsigned x) {
   x = ((x & 0x55555555) <<  1) | ((x >>  1) & 0x55555555);
   x = ((x & 0x33333333) <<  2) | ((x >>  2) & 0x33333333);
   x = ((x & 0x0F0F0F0F) <<  4) | ((x >>  4) & 0x0F0F0F0F);
   x = (x << 24) | ((x & 0xFF00) << 8) |
       ((x >> 8) & 0xFF00) | (x >> 24);
   return x;
}

// Función para realizar el calculo de redundacia con 32 bits
static unsigned int crc32(unsigned char *message) {
   int i, j;
   unsigned int byte, crc;

   i = 0;
   crc = 0xFFFFFFFF;
   while (message[i] != 0) {
      byte = message[i];            // Get next byte.
      byte = reverse(byte);         // 32-bit reversal.
      for (j = 0; j <= 7; j++) {    // Do eight times.
         if ((int)(crc ^ byte) < 0)
              crc = (crc << 1) ^ 0x04C11DB7;
         else crc = crc << 1;
         byte = byte << 1;          // Ready next msg bit.
      }
      i = i + 1;
   }
   return reverse(~crc);
}

// Traducción de un número sin signo a un array de caracteres.
static void c2h(char* texto, unsigned int num) {
	printk(KERN_DEBUG NOMB_DRIVER": El código CRC en decimal es: %u", num);
	unsigned int n = num;
	unsigned int i = 0;
	unsigned char temp[TAM_CADENA];

	while(n > 0) {
		unsigned int r = n % 16;

		if(r < 10) {
			temp[i] = ('0' + r);
		} else {
			temp[i] = ('a' + r - 10);
		}
		//printk(KERN_DEBUG DRIVER_CLASS": %c", temp[i]);
		n /= 16;
		++i;
	}
	unsigned int k = 0;
	for(int i=TAM_CADENA-1; i > 0; --i) {
		texto[k] = temp[i-1];
		k++;
	}
	texto[k]='\0';
}

// Funciones de operación
//  Función de lectura
//  La lectura va devolviendo según la longitud
//  de len la cantidad de bytes por lectura, hasta terminar con esta
static ssize_t driver_lee(
	struct file* filep,
	char* bufer,
	size_t len,
	loff_t* offset) {

	// Comprueba que el offset no supere a los bytes en búfer,
	// esto indica que se ha terminado de leer los datos del driver
	if((bytesEnBufer - (*offset)) > 0) {

		size_t bytesSinCopiar = 0;

		// Para realizar copias enteras es el valor len
		// pero por si alguna razón no es multiplo o hay menos bytes
		// que len se usa len - hexadecimal+(*offset) para el resto
		unsigned long bytesACopiar = len > TAM_CADENA - (*offset) ?
						TAM_CADENA - (*offset) :
						len;

		bytesSinCopiar = copy_to_user(bufer,
			hexadecimal+(*offset),
			len);

		if(bytesSinCopiar) {
			pr_warn("%s: Error mientras se copiaban %zu bytes a la "
			"zona de usuario\n", NOMB_DRIVER, bytesSinCopiar);
			return -EFAULT;
		}
		(*offset)+=len;
		return bytesACopiar;
	}
	pr_info(NOMB_DRIVER": Todos los bytes leidos\n");
	pr_info(NOMB_DRIVER": Desplazamiento final %lli\n", (*offset));
	return 0;
}

//  Función de escritura
//  La escritura sirve para el string de entrada
static ssize_t driver_escribe(struct file *file,
	const char __user *bufer,
	size_t length,
	loff_t *offset) {

	size_t bytesSinCopiar = 0;
	char copiaBufer[LONG_BUF] = {0};

	bytesSinCopiar = copy_from_user(copiaBufer, bufer,length);
	if(bytesSinCopiar) {
		pr_warn("%s: Error mientras se copiaban %zu bytes desde la "
		"zona de usuario\n", NOMB_DRIVER, bytesSinCopiar);
		return -EFAULT;
	}

	printk(KERN_NOTICE NOMB_DRIVER": La palabra es:");
	for(int i = 0; i < length; i++) {
		printk(KERN_CONT "%c",copiaBufer[i]);
	}
	printk(KERN_CONT "\n");

	pr_info(NOMB_DRIVER": Escribiendo en el driver");

	// Resetea la cadena hexadecimal
	memset(hexadecimal, 0, TAM_CADENA);

	// Guarda en hexadecimal el mensaje codificado
	c2h(hexadecimal, crc32(copiaBufer));

	printk(KERN_NOTICE NOMB_DRIVER": El hash es: 0x");
	for(int i = 0; i < TAM_CADENA-1; i++) {
		printk(KERN_CONT "%c",hexadecimal[i]);
	}
	printk(KERN_CONT "\n");

	bytesEnBufer = TAM_CADENA;

	return length;
}

//  Definición de las operaciones con archivos para el kernel
static struct file_operations operaciones_driver = {
	.owner = THIS_MODULE,
	.read = driver_lee,
	.write = driver_escribe,
};

static struct miscdevice mi_dispo = {
	.minor = MISC_DYNAMIC_MINOR,
	.name  = NOMB_DRIVER,
	.fops  = &operaciones_driver,
	.mode  = 0666,
};

static int inicio_dispo(void) {
	misc_register(&mi_dispo);
	pr_info(NOMB_DRIVER": Cargado en el núcleo");
	return 0;
}

static void fin_dispo(void) {
	misc_deregister(&mi_dispo);
	pr_info(NOMB_DRIVER": Descargado del núcleo");
}

// Iniciar esta función al cargarla en el kernel
module_init(inicio_dispo)

// Iniciar esta función al descargarlo del kernel
module_exit(fin_dispo)

// Licencia del módulo
MODULE_DESCRIPTION("Un simple driver para calcular el código de redundancia "
			"en CRC32");
MODULE_AUTHOR("Daniel Armenteros Martínez");
MODULE_LICENSE("GPL");


MODULE_VERSION("1.0");
