# Módulo del núcleo de Linux para el cálculo en CRC32
Módulo para saber el código de redundancia de una cadena de caracteres 
 en Linux a través de la shell o de un programa en C.
 
El modulo es un dispositivo de caracteres con permisos para todos los
usuarios de lectura y escritura.

El búfer del módulo está limitado a 512 caracteres o bytes de entrada.

El módulo es conforme al ISO C90 y ha sido comprobado que compila y 
funciona para los núcleos de Linux v5.15.0 y v6.15.0

## Compilar
Para instalar todo lo necesario para compilar este módulo es:

> En la distribución de Ubuntu/Debian o derivados: <br>
> <code>sudo apt-get install build-essential kmod linux- &#768;uname -r &#768;</code>

> En Arch o derivados: <br>
`sudo pacman -S gcc kmod linux-headers`

Se usa `make` en la carpeta donde se encuentre Makefile y el archivo CRCDriver.c
y se compilara el módulo para tu versión de Linux usada en el sistema

## Uso
Como este no es un driver que sea crítico para el sistema ni nada por
el estilo lo puede usar cualquier usuario del sistema.

Con los comandos del shell se puede probar el driver

`sudo insmod CRC32Hasher.ko`

![sudo insmod CRC32Hasher](Imágenes/1 insmod.png)

`echo -n probando > /dev/CRC32Hasher`

![echo -n probando > /dev/CRC32Hasher](Imágenes/2 echo.png)

`read X < /dev/CRC32Hasher`

![read X < /dev/CRC32Hasher](Imágenes/3 read.png)

Salida `echo`

![echo $X](Imágenes/4 echo X.png)

Salida en `dmesg`

![sudo dmesg](Imágenes/5 dmesg.png)

Salida de `rhash` para comprobar su correctitud

![echo -n probando | rhash -C -](Imágenes/6 rhash probando.png)

Descargar el driver con `rmmod`

![sudo rmmod CRCDriver](Imágenes/7 rmmod.png)

## Módulo compilado
El módulo .ko esta compilado para el núcleo de Linux v6.5.0-15-generic y v5.15.0-25-generic, para los 
que quieran probarlo sin compilar el módulo.
